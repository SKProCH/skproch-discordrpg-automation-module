# SKProCH' Automation module for Discord RPG Game

A simple application that will show you how you can automate passive raising skills and level upping in a text game - the Discord RPG.


The **project presented only for reference** and is **not recommended for permanent use**, in order to avoid unpleasant situations.  
Using it in Discord on DiscordRPG # 0366 you break the rules and your account can be blocked in the game. ***Use at your own risk!***


*More detailed design for README.md will be later*