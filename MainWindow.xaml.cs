﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using System.Net;
using System.IO;
using System.Web;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using WindowsInput.Native;
using WindowsInput;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Reflection;
using DotNetBrowser;
using DotNetBrowser.WPF;

namespace DRPGMacros
{
    public class Config
    {
        public Config()
        {
            CraftList = new List<Items>();
            SellList = new List<Items>();
        }

        public bool AutoCraft = true;
        public bool AutoSell { get; set; }
        public List<Items> CraftList { get; set; }
        public List<Items> SellList { get; set; }

        public class Items
        {
            Items()
            {
                ItemName = "";
                ItemCount = -1;
            }

            public string ItemName { get; set; }
            public int ItemCount { get; set; }
        }
    }

    public partial class MainWindow : Window
    {
        /// <summary>
        ///     Retrieves a handle to the foreground window (the window with which the user is currently working). The system
        ///     assigns a slightly higher priority to the thread that creates the foreground window than it does to other threads.
        ///     <para>See https://msdn.microsoft.com/en-us/library/windows/desktop/ms633505%28v=vs.85%29.aspx for more information.</para>
        /// </summary>
        /// <returns>
        ///     C++ ( Type: Type: HWND )<br /> The return value is a handle to the foreground window. The foreground window
        ///     can be NULL in certain circumstances, such as when a window is losing activation.
        /// </returns>
        [DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        static extern int GetWindowText(IntPtr hWnd, StringBuilder lpString, int nMaxCount);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int GetWindowTextLength(IntPtr hWnd);

        [DllImport("User32", CallingConvention = CallingConvention.StdCall)]
        public static extern void mouse_event(MouseEvent dwFlags, long dx, long dy, long cButtons, long dwExtraInfo);
        public enum MouseEvent
        {
            MOUSEEVENTF_LEFTDOWN = 0x02,
            MOUSEEVENTF_LEFTUP = 0x04,
            MOUSEEVENTF_RIGHTDOWN = 0x08,
            MOUSEEVENTF_RIGHTUP = 0x10,
        }

        [DllImport("User32.dll")]
        private static extern bool SetForegroundWindow(IntPtr hWnd);

        [DllImport("user32.dll")]
        static extern void keybd_event(byte bVk, byte bScan, uint dwFlags, uint dwExtraInfo);

        public static MainWindow form = new MainWindow();
        public static Config cfg = new Config();
        public MainWindow()
        {
            //System.Threading.Thread.CurrentThread.CurrentUICulture = System.Globalization.CultureInfo.CreateSpecificCulture("Neutral");
            InitializeComponent();

            Properties.Settings.Default.Reload();
            this.Width = SystemParameters.WorkArea.Width;
            this.Height = SystemParameters.WorkArea.Height;
            this.Focus();
            this.Top = 0;
            this.Left = 0;
            Browser.Height = this.Height / 2;
            Browser.Browser.StartLoadingFrameEvent += new DotNetBrowser.Events.StartLoadingFrameHandler((x0, y0) =>
            {
                form.Dispatcher.Invoke(() =>
                {
                    CurrentURLField.Content = Browser.Browser.URL;
                });
            });

            string[] args = Environment.GetCommandLineArgs();
            foreach (var item in args)
            {
                if (item == "$Recovery")
                {
                    Properties.Settings.Default.Adventures = false;
                    Button_Click(this, new RoutedEventArgs());
                }
            }

            form = this;
            form.Closing += new System.ComponentModel.CancelEventHandler((x0, y0) =>
            {
                Properties.Settings.Default.NickName = NicknameField.Text;
                try
                {
                }
                catch (Exception) { }
                Environment.Exit(666);
            });
            NicknameField.Text = Properties.Settings.Default.NickName;
        }

        static bool ThreadsRunning = false;

        static Thread th300 = new Thread(Actions_300);
        static Thread th600 = new Thread(Actions_600);
        static Thread thAdv = new Thread(Adventures);
        static Thread thCraft = new Thread(Craft);
        static Thread thSell = new Thread(Sell);

        static bool IsRunning = false;
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.Save();
            if (IsRunning)
            {
                th300.Abort();
                th300.Join();
                th300 = new Thread(Actions_300);
                th600.Abort();
                th600.Join();
                th600 = new Thread(Actions_600);
                thAdv.Abort();
                thAdv.Join();
                thAdv = new Thread(Adventures);

                StartStopButton.Content = "Начать";
                IsRunning = false;
                NicknameField.IsReadOnly = false;
                Process.Start(System.Windows.Forms.Application.ExecutablePath);
                Environment.Exit(666);
            }
            else
            {
                StartStopButton.Content = "Закончить";
                IsRunning = true;
                NicknameField.IsReadOnly = true;
                Properties.Settings.Default.NickName = NicknameField.Text;
                Properties.Settings.Default.Adventures = (bool)AdvF.IsChecked;
                Properties.Settings.Default.Chop = (bool)ChopF.IsChecked;
                Properties.Settings.Default.Fish = (bool)FishF.IsChecked;
                Properties.Settings.Default.Forage = (bool)ForageF.IsChecked;
                Properties.Settings.Default.GoldTracking = (bool)GoldF.IsChecked;
                Properties.Settings.Default.HPPercents = XPPercentField.Text;
                Properties.Settings.Default.Mine = (bool)MineF.IsChecked;
            }
        }

        private static string GetFocusedWindowTitle()
        {
            IntPtr handle = GetForegroundWindow();
            int length = GetWindowTextLength(handle);
            StringBuilder text = new StringBuilder(length + 1);
            GetWindowText(handle, text, text.Capacity);
            return text.ToString();
        }

        private static void Actions_300()
        {
            while (true)
            {
                
            }
        }

        private static void Actions_600()
        {

        }

        private static void Adventures()
        {

        }

        private static void Craft()
        {

        }

        private static void Sell()
        {

        }

        private void WindowMove(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        //Exit
        private async void Button_Click_1(object sender, RoutedEventArgs e)
        {
            await Task.Delay(TimeSpan.FromMilliseconds(300));
            Environment.Exit(666);
        }

        //Button for URL saving
        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            URLField.Text = Browser.Browser.URL;
        }

        private void TestButton_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.MessageBox.Show("Кнопка нажата");
        }
    }
}
